import 'dart:io';

// ignore: depend_on_referenced_packages
import 'package:csv/csv.dart';
import 'package:daten_app/pb/hermann_daten/v1/hermann_daten.pb.dart' as pb;
import 'package:encrypt/encrypt.dart' as encrypt;

void main(final List<String> arguments) {
  if (arguments.length != 2) {
    throw Exception('Specify only the input and output files');
  }

  final csvData = const CsvToListConverter(eol: '\n').convert(
    File(arguments[0]).readAsStringSync().replaceAll('\r', ''),
  );

  final data = pb.QRData(
    version: 1,
    pupilDetails: csvData.map(
      (final row) => pb.PupilDetails(
        id: row[0] as String,
        name: row[1] as String,
        group: row[2] as String,
        schoolYear: row[3] as String,
      ),
    ),
  );

  final key = encrypt.Key.fromUtf8(File('assets/keys/keyaes256cbc.txt').readAsStringSync());
  final iv = encrypt.IV.fromUtf8(File('assets/keys/ivaes256cbc.txt').readAsStringSync());
  final encrypted = encrypt.Encrypter(
    encrypt.AES(
      key,
      mode: encrypt.AESMode.cbc,
    ),
  )
      .encryptBytes(
        data.writeToBuffer(),
        iv: iv,
      )
      .bytes;
  File(arguments[1]).writeAsBytesSync(encrypted);
}
