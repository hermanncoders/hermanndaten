part of '../daten_app.dart';

String? validateNotEmpty(final BuildContext context, final String? input) {
  if (input == null || input == '') {
    return AppLocalizations.of(context).errorEmptyField;
  }

  return null;
}
