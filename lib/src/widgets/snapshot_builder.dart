part of '../daten_app.dart';

class SnapshotBuilder<T> extends StatelessWidget {
  const SnapshotBuilder({
    required this.snapshot,
    required this.onRetry,
    required this.builder,
    super.key,
  });

  final AsyncSnapshot<T> snapshot;
  final VoidCallback onRetry;
  final Widget Function(BuildContext context, T data) builder;

  @override
  Widget build(final BuildContext context) {
    if (snapshot.hasData) {
      return builder(context, snapshot.data as T);
    }

    if (snapshot.hasError) {
      return ErrorWidget(
        error: snapshot.error,
        onRetry: onRetry,
      );
    }

    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
