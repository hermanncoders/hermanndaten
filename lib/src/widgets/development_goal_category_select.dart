part of '../daten_app.dart';

class DevelopmentGoalCategorySelect extends StatefulWidget {
  const DevelopmentGoalCategorySelect({
    required this.categories,
    required this.validate,
    required this.onCategorySelected,
    super.key,
  });

  final List<DevelopmentGoalCategory> categories;
  final bool validate;
  final Function(int? categoryId) onCategorySelected;

  @override
  State<DevelopmentGoalCategorySelect> createState() => _DevelopmentGoalCategorySelectState();
}

class _DevelopmentGoalCategorySelectState extends State<DevelopmentGoalCategorySelect> {
  int? selectedId;

  @override
  Widget build(final BuildContext context) => Column(
        children: [
          DropdownButtonFormField<DevelopmentGoalCategory?>(
            isExpanded: true,
            value: selectedId != null
                ? widget.categories.singleWhere((final category) => category.id == selectedId)
                : null,
            items: [
              null,
              ...widget.categories,
            ]
                .map(
                  (final category) => DropdownMenuItem<DevelopmentGoalCategory?>(
                    value: category,
                    child: Text(
                      category?.name ?? AppLocalizations.of(context).developmentGoalCategorySelectNoCategory,
                      style: category == null
                          ? const TextStyle(
                              color: Colors.black38,
                            )
                          : null,
                    ),
                  ),
                )
                .toList(),
            onChanged: (final category) {
              setState(() {
                selectedId = category?.id;
              });
              widget.onCategorySelected(category?.id);
            },
            validator: widget.validate ? (final input) => validateNotEmpty(context, input?.name) : null,
          ),
          Builder(
            builder: (final context) {
              if (selectedId == null) {
                return Container();
              }
              final subCategories =
                  widget.categories.singleWhere((final category) => category.id == selectedId).subCategories;
              if (subCategories.isEmpty) {
                return Container();
              }

              return DevelopmentGoalCategorySelect(
                key: Key(selectedId!.toString()),
                categories: subCategories,
                validate: false,
                onCategorySelected: (final categoryId) {
                  widget.onCategorySelected(categoryId ?? selectedId);
                },
              );
            },
          ),
        ],
      );
}
