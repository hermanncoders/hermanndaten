part of '../daten_app.dart';

class ErrorWidget extends StatelessWidget {
  const ErrorWidget({
    required this.error,
    required this.onRetry,
    super.key,
  });

  final dynamic error;
  final VoidCallback onRetry;

  static void showSnackbar(final BuildContext context, final dynamic error) {
    final details = _getErrorDetails(context, error);

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(details.text),
        action: details.isUnauthorized
            ? SnackBarAction(
                label: AppLocalizations.of(context).loginAgain,
                onPressed: () async => _openLoginPage(context),
              )
            : null,
      ),
    );
  }

  @override
  Widget build(final BuildContext context) => error == null
      ? Container()
      : Padding(
          padding: const EdgeInsets.all(5),
          child: Builder(
            builder: (final context) {
              final details = _getErrorDetails(context, error);

              return Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.error_outline,
                        size: 30,
                        color: Colors.red,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Flexible(
                        child: Text(
                          details.text,
                          style: const TextStyle(
                            color: Colors.red,
                          ),
                        ),
                      ),
                    ],
                  ),
                  if (details.isUnauthorized) ...[
                    ElevatedButton(
                      onPressed: () async => _openLoginPage(context),
                      child: Text(AppLocalizations.of(context).loginAgain),
                    ),
                  ] else ...[
                    ElevatedButton(
                      onPressed: onRetry,
                      child: Text(AppLocalizations.of(context).retry),
                    ),
                  ],
                ],
              );
            },
          ),
        );

  static _ErrorDetails _getErrorDetails(final BuildContext context, final dynamic exception) {
    if (exception is String) {
      return _ErrorDetails(
        text: exception,
      );
    }

    return _ErrorDetails(
      text: AppLocalizations.of(context).errorUnknown,
    );
  }

  static Future _openLoginPage(final BuildContext context) async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (final context) => const LoginPage(),
      ),
    );
  }
}

class _ErrorDetails {
  _ErrorDetails({
    required this.text,
    this.isUnauthorized = false,
  });

  final String text;
  final bool isUnauthorized;
}
