// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'session.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Session _$SessionFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    allowedKeys: const ['username', 'jwt'],
  );
  return Session(
    username: json['username'] as String?,
    jwt: json['jwt'] as String?,
  );
}

Map<String, dynamic> _$SessionToJson(Session instance) => <String, dynamic>{
      'username': instance.username,
      'jwt': instance.jwt,
    };
