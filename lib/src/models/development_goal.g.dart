// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'development_goal.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DevelopmentGoal _$DevelopmentGoalFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    allowedKeys: const ['id', 'categoryID', 'creator', 'created', 'achieved', 'description', 'strategies', 'checks'],
  );
  return DevelopmentGoal(
    id: json['id'] as int?,
    categoryID: json['categoryID'] as int,
    creator: json['creator'] as String,
    created: DateTime.parse(json['created'] as String),
    achieved: json['achieved'] as bool,
    description: json['description'] as String,
    strategies: (json['strategies'] as List<dynamic>).map((e) => e as String).toList(),
    checks:
        (json['checks'] as List<dynamic>).map((e) => DevelopmentGoalCheck.fromJson(e as Map<String, dynamic>)).toList(),
  );
}

Map<String, dynamic> _$DevelopmentGoalToJson(DevelopmentGoal instance) => <String, dynamic>{
      'id': instance.id,
      'categoryID': instance.categoryID,
      'creator': instance.creator,
      'created': instance.created.toIso8601String(),
      'achieved': instance.achieved,
      'description': instance.description,
      'strategies': instance.strategies,
      'checks': instance.checks.map((e) => e.toJson()).toList(),
    };
