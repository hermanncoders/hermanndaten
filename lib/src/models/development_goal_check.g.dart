// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'development_goal_check.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DevelopmentGoalCheck _$DevelopmentGoalCheckFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    allowedKeys: const ['id', 'creator', 'created', 'comment'],
  );
  return DevelopmentGoalCheck(
    id: json['id'] as int,
    creator: json['creator'] as String,
    created: DateTime.parse(json['created'] as String),
    comment: json['comment'] as String,
  );
}

Map<String, dynamic> _$DevelopmentGoalCheckToJson(DevelopmentGoalCheck instance) => <String, dynamic>{
      'id': instance.id,
      'creator': instance.creator,
      'created': instance.created.toIso8601String(),
      'comment': instance.comment,
    };
