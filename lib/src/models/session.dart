import 'package:json_annotation/json_annotation.dart';

part 'session.g.dart';

@JsonSerializable()
class Session {
  Session({
    this.username,
    this.jwt,
  });

  factory Session.fromJson(final Map<String, dynamic> json) => _$SessionFromJson(json);
  Map<String, dynamic> toJson() => _$SessionToJson(this);

  final String? username;

  final String? jwt;

  bool get isAuthenticated => username != null && jwt != null;
}
