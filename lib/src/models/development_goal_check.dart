import 'package:json_annotation/json_annotation.dart';

part 'development_goal_check.g.dart';

@JsonSerializable()
class DevelopmentGoalCheck {
  DevelopmentGoalCheck({
    required this.id,
    required this.creator,
    required this.created,
    required this.comment,
  });

  factory DevelopmentGoalCheck.fromJson(final Map<String, dynamic> json) => _$DevelopmentGoalCheckFromJson(json);
  Map<String, dynamic> toJson() => _$DevelopmentGoalCheckToJson(this);

  final int id;

  final String creator;

  final DateTime created;

  final String comment;
}
