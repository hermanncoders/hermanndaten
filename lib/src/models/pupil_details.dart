import 'package:json_annotation/json_annotation.dart';

part 'pupil_details.g.dart';

@JsonSerializable()
class PupilDetails {
  PupilDetails({
    required this.id,
    required this.name,
    required this.group,
    required this.schoolYear,
  });

  factory PupilDetails.fromJson(final Map<String, dynamic> json) => _$PupilDetailsFromJson(json);
  Map<String, dynamic> toJson() => _$PupilDetailsToJson(this);

  final String id;

  final String name;

  final String group;

  final String schoolYear;
}
