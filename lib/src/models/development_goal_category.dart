import 'package:json_annotation/json_annotation.dart';

part 'development_goal_category.g.dart';

@JsonSerializable()
class DevelopmentGoalCategory {
  DevelopmentGoalCategory({
    required this.id,
    required this.name,
    this.subCategories = const [],
  });

  factory DevelopmentGoalCategory.fromJson(final Map<String, dynamic> json) => _$DevelopmentGoalCategoryFromJson(json);
  Map<String, dynamic> toJson() => _$DevelopmentGoalCategoryToJson(this);

  final int id;

  final String name;

  @JsonKey(name: 'categories')
  final List<DevelopmentGoalCategory> subCategories;
}

extension DevelopmentGoalCategoriesFind on List<DevelopmentGoalCategory> {
  DevelopmentGoalCategory? find(final int id) {
    for (final category in this) {
      if (category.id == id) {
        return category;
      }

      final result = category.subCategories.find(id);
      if (result != null) {
        return result;
      }
    }

    return null;
  }
}
