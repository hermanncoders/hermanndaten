import 'package:daten_app/src/models/development_goal_check.dart';
import 'package:json_annotation/json_annotation.dart';

part 'development_goal.g.dart';

@JsonSerializable()
class DevelopmentGoal {
  DevelopmentGoal({
    required this.id,
    required this.categoryID,
    required this.creator,
    required this.created,
    required this.achieved,
    required this.description,
    required this.strategies,
    required this.checks,
  });

  factory DevelopmentGoal.fromJson(final Map<String, dynamic> json) => _$DevelopmentGoalFromJson(json);
  Map<String, dynamic> toJson() => _$DevelopmentGoalToJson(this);

  final int? id;

  final int categoryID;

  final String creator;

  final DateTime created;

  final bool achieved;

  final String description;

  final List<String> strategies;

  final List<DevelopmentGoalCheck> checks;
}
