import 'package:daten_app/src/models/development_goal.dart';
import 'package:json_annotation/json_annotation.dart';

part 'pupil.g.dart';

@JsonSerializable()
class Pupil {
  Pupil({
    required this.id,
    required this.developmentGoals,
  });

  factory Pupil.fromJson(final Map<String, dynamic> json) => _$PupilFromJson(json);
  Map<String, dynamic> toJson() => _$PupilToJson(this);

  final String id;

  final List<DevelopmentGoal> developmentGoals;
}
