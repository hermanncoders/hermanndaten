// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pupil_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PupilDetails _$PupilDetailsFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    allowedKeys: const ['id', 'name', 'group', 'schoolYear'],
  );
  return PupilDetails(
    id: json['id'] as String,
    name: json['name'] as String,
    group: json['group'] as String,
    schoolYear: json['schoolYear'] as String,
  );
}

Map<String, dynamic> _$PupilDetailsToJson(PupilDetails instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'group': instance.group,
      'schoolYear': instance.schoolYear,
    };
