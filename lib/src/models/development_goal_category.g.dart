// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'development_goal_category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DevelopmentGoalCategory _$DevelopmentGoalCategoryFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    allowedKeys: const ['id', 'name', 'categories'],
  );
  return DevelopmentGoalCategory(
    id: json['id'] as int,
    name: json['name'] as String,
    subCategories: (json['categories'] as List<dynamic>?)
            ?.map((e) => DevelopmentGoalCategory.fromJson(e as Map<String, dynamic>))
            .toList() ??
        const [],
  );
}

Map<String, dynamic> _$DevelopmentGoalCategoryToJson(DevelopmentGoalCategory instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'categories': instance.subCategories.map((e) => e.toJson()).toList(),
    };
