// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pupil.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Pupil _$PupilFromJson(Map<String, dynamic> json) {
  $checkKeys(
    json,
    allowedKeys: const ['id', 'developmentGoals'],
  );
  return Pupil(
    id: json['id'] as String,
    developmentGoals: (json['developmentGoals'] as List<dynamic>)
        .map((e) => DevelopmentGoal.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$PupilToJson(Pupil instance) => <String, dynamic>{
      'id': instance.id,
      'developmentGoals': instance.developmentGoals.map((e) => e.toJson()).toList(),
    };
