part of '../daten_app.dart';

class UpsertDevelopmentGoalPage extends StatefulWidget {
  const UpsertDevelopmentGoalPage({
    required this.existingDevelopmentGoal,
    required this.categories,
    super.key,
  });

  final DevelopmentGoal? existingDevelopmentGoal;
  final List<DevelopmentGoalCategory> categories;

  @override
  State<UpsertDevelopmentGoalPage> createState() => _UpsertDevelopmentGoalPageState();
}

class _UpsertDevelopmentGoalPageState extends State<UpsertDevelopmentGoalPage> {
  final formKey = GlobalKey<FormState>();
  late int? selectedCategoryId = widget.existingDevelopmentGoal?.categoryID;
  late bool achieved = widget.existingDevelopmentGoal?.achieved ?? false;
  late final descriptionController = TextEditingController()..text = widget.existingDevelopmentGoal?.description ?? '';
  late final strategyControllers = <TextEditingController>[
    if (widget.existingDevelopmentGoal != null) ...[
      for (final strategy in widget.existingDevelopmentGoal!.strategies) ...[
        TextEditingController()..text = strategy,
      ],
    ] else ...[
      TextEditingController(),
    ],
  ];

  @override
  Widget build(final BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(
            widget.existingDevelopmentGoal == null
                ? AppLocalizations.of(context).upsertDevelopmentGoalEditTitle
                : AppLocalizations.of(context).upsertDevelopmentGoalEditTitle,
          ),
        ),
        body: Form(
          key: formKey,
          child: ListView(
            padding: const EdgeInsets.all(10),
            children: [
              Text(
                AppLocalizations.of(context).upsertDevelopmentGoalCategory,
                style: Theme.of(context).textTheme.headline5,
              ),
              DevelopmentGoalCategorySelect(
                categories: widget.categories,
                validate: true,
                onCategorySelected: (final categoryId) {
                  setState(() {
                    selectedCategoryId = categoryId;
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                AppLocalizations.of(context).upsertDevelopmentGoalDescription,
                style: Theme.of(context).textTheme.headline5,
              ),
              TextFormField(
                controller: descriptionController,
                validator: (final input) => validateNotEmpty(context, input),
                autofocus: true,
                keyboardType: TextInputType.multiline,
                maxLines: null,
                minLines: 5,
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                AppLocalizations.of(context).upsertDevelopmentGoalStrategies,
                style: Theme.of(context).textTheme.headline5,
              ),
              for (var i = 0; i < strategyControllers.length; i++) ...[
                Builder(
                  builder: (final context) {
                    final controller = strategyControllers[i];
                    return Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text('${i + 1}: '),
                        Expanded(
                          child: TextFormField(
                            controller: controller,
                            validator: (final input) => validateNotEmpty(context, input),
                          ),
                        ),
                        IconButton(
                          onPressed: () {
                            setState(() {
                              strategyControllers.removeAt(i);
                            });
                          },
                          icon: const Icon(Icons.delete),
                        ),
                      ],
                    );
                  },
                ),
              ],
              const SizedBox(
                height: 10,
              ),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      strategyControllers.add(TextEditingController());
                    });
                  },
                  child: Text(AppLocalizations.of(context).upsertDevelopmentGoalStrategiesAdd),
                ),
              ),
              if (widget.existingDevelopmentGoal != null) ...[
                const SizedBox(
                  height: 10,
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      achieved = !achieved;
                    });
                  },
                  child: Row(
                    children: [
                      Checkbox(
                        value: achieved,
                        onChanged: (final value) {
                          setState(() {
                            achieved = value!;
                          });
                        },
                      ),
                      Text(
                        AppLocalizations.of(context).upsertDevelopmentGoalAchieved,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ],
                  ),
                ),
              ],
              const SizedBox(
                height: 50,
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: ElevatedButton(
                  onPressed: () {
                    if (!formKey.currentState!.validate()) {
                      return;
                    }
                    Navigator.of(context).pop(
                      UpsertDevelopmentGoalDetails(
                        category: widget.categories.find(selectedCategoryId!)!,
                        achieved: achieved,
                        description: descriptionController.text,
                        strategies: strategyControllers.map((final c) => c.text).toList(),
                        checks: [], // TODO
                      ),
                    );
                  },
                  child: Text(AppLocalizations.of(context).upsertDevelopmentGoalSubmit),
                ),
              ),
            ],
          ),
        ),
      );
}

class UpsertDevelopmentGoalDetails {
  UpsertDevelopmentGoalDetails({
    required this.category,
    required this.achieved,
    required this.description,
    required this.strategies,
    required this.checks,
  });

  final DevelopmentGoalCategory category;

  final bool achieved;

  final String description;

  final List<String> strategies;

  final List<DevelopmentGoalCheck> checks;
}
