part of '../daten_app.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({
    super.key,
  });

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final scrollController = ScrollController();
  final formKey = GlobalKey<FormState>();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  final usernameFocus = FocusNode();
  final passwordFocus = FocusNode();

  final loginBloc = LoginBloc();

  @override
  void initState() {
    super.initState();

    usernameFocus.addListener(scrollToBottom);

    loginBloc.errors.listen((final error) {
      ErrorWidget.showSnackbar(context, error);
    });
    loginBloc.session.listen((final session) {
      if (session.isAuthenticated) {
        Provider.of<SessionBloc>(context, listen: false).set(session);
      } else {
        passwordController.text = '';
        passwordFocus.requestFocus();

        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(AppLocalizations.of(context).errorCredentialsWrong),
          ),
        );
      }
    });
  }

  @override
  void dispose() {
    usernameFocus.dispose();
    passwordFocus.dispose();
    super.dispose();
  }

  void login() {
    if (formKey.currentState!.validate()) {
      loginBloc.login(
        usernameController.text,
        passwordController.text,
      );
    }
  }

  Future scrollToBottom() async {
    // Wait for keyboard to appear
    await Future.delayed(const Duration(milliseconds: 500));
    await scrollController.animateTo(
      scrollController.position.maxScrollExtent,
      duration: const Duration(milliseconds: 250),
      curve: Curves.easeInOut,
    );
  }

  @override
  Widget build(final BuildContext context) => Scaffold(
        body: Center(
          child: ListView(
            controller: scrollController,
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
            children: <Widget>[
              Text(
                AppLocalizations.of(context).title,
                style: Theme.of(context).textTheme.headline3!.copyWith(
                      color: Colors.black,
                    ),
                textAlign: TextAlign.center,
              ),
              Image.asset('assets/hermanndaten.png'),
              Form(
                key: formKey,
                child: Column(
                  children: [
                    TextFormField(
                      decoration: InputDecoration(
                        hintText: AppLocalizations.of(context).loginUsername,
                      ),
                      validator: (final input) => validateNotEmpty(context, input),
                      onFieldSubmitted: (final input) {
                        passwordFocus.requestFocus();
                      },
                      controller: usernameController,
                      focusNode: usernameFocus,
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        hintText: AppLocalizations.of(context).loginPassword,
                      ),
                      validator: (final input) => validateNotEmpty(context, input),
                      onFieldSubmitted: (final input) {
                        login();
                      },
                      controller: passwordController,
                      focusNode: passwordFocus,
                      obscureText: true,
                    ),
                  ],
                ),
              ),
              ElevatedButton(
                onPressed: login,
                child: Text(AppLocalizations.of(context).loginSubmit),
              ),
            ]
                .intersperse(
                  const SizedBox(
                    height: 10,
                  ),
                )
                .toList(),
          ),
        ),
      );
}
