part of '../daten_app.dart';

class PupilOverviewPage extends StatelessWidget {
  const PupilOverviewPage({
    required this.categories,
    required this.pupilDetails,
    required this.pupilsBloc,
    super.key,
  });

  final List<DevelopmentGoalCategory> categories;
  final PupilDetails pupilDetails;
  final PupilsBloc pupilsBloc;

  @override
  Widget build(final BuildContext context) => StreamBuilder<List<Pupil>>(
        stream: pupilsBloc.pupils,
        builder: (final context, final pupilsSnapshot) => Scaffold(
          appBar: AppBar(
            title: Text(pupilDetails.name),
          ),
          floatingActionButton: pupilsSnapshot.hasData
              ? FloatingActionButton(
                  onPressed: () async {
                    final details = await Navigator.of(context).push<UpsertDevelopmentGoalDetails>(
                      MaterialPageRoute(
                        builder: (final context) => UpsertDevelopmentGoalPage(
                          existingDevelopmentGoal: null,
                          categories: categories,
                        ),
                      ),
                    );
                    if (details == null) {
                      return;
                    }

                    pupilsBloc.addDevelopmentGoal(
                      pupilDetails: pupilDetails,
                      category: details.category,
                      description: details.description,
                      strategies: details.strategies,
                    );
                  },
                  child: const Icon(Icons.add),
                )
              : null,
          body: Builder(
            builder: (final context) {
              if (pupilsSnapshot.hasError) {
                return ErrorWidget(
                  error: pupilsSnapshot.error,
                  onRetry: pupilsBloc.refresh,
                );
              }
              if (!pupilsSnapshot.hasData) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              final pupil = pupilsSnapshot.data!.singleWhere((final pupil) => pupilDetails.id == pupil.id);
              return ListView(
                children: [
                  for (final developmentGoal in pupil.developmentGoals) ...[
                    ListTile(
                      title: Text(categories.find(developmentGoal.categoryID)!.name),
                      subtitle: Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: developmentGoal.creator,
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black54,
                              ),
                            ),
                            const TextSpan(text: ' - '),
                            TextSpan(
                              text: dateFormat.format(developmentGoal.created),
                            ),
                          ],
                        ),
                      ),
                      onTap: () async {
                        final details = await Navigator.of(context).push<UpsertDevelopmentGoalDetails>(
                          MaterialPageRoute(
                            builder: (final context) => UpsertDevelopmentGoalPage(
                              existingDevelopmentGoal: developmentGoal,
                              categories: categories,
                            ),
                          ),
                        );
                        if (details == null) {
                          return;
                        }

                        pupilsBloc.updateDevelopmentGoal(
                          pupilDetails: pupilDetails,
                          goal: developmentGoal,
                          achieved: details.achieved,
                          description: details.description,
                          strategies: details.strategies,
                          checks: details.checks,
                        );
                      },
                    ),
                  ],
                ],
              );
            },
          ),
        ),
      );
}
