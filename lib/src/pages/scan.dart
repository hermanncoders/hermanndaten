part of '../daten_app.dart';

class ScanPage extends StatefulWidget {
  const ScanPage({
    required this.pupilsDetailsBloc,
    super.key,
  });

  final PupilsDetailsBloc pupilsDetailsBloc;

  @override
  State<ScanPage> createState() => _ScanPageState();
}

class _ScanPageState extends State<ScanPage> {
  final controller = MobileScannerController();

  @override
  void initState() {
    super.initState();

    controller.barcodes.listen(processBarCode);
  }

  Future processBarCode(final Barcode barcode) async {
    if (barcode.rawBytes == null) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(AppLocalizations.of(context).scanError),
        ),
      );
      return;
    }

    try {
      final key = encrypt.Key.fromUtf8(await rootBundle.loadString('assets/keys/keyaes256cbc.txt'));
      final iv = encrypt.IV.fromUtf8(await rootBundle.loadString('assets/keys/ivaes256cbc.txt'));
      final decrypted = encrypt.Encrypter(
        encrypt.AES(
          key,
          mode: encrypt.AESMode.cbc,
        ),
      ).decryptBytes(
        encrypt.Encrypted(barcode.rawBytes!),
        iv: iv,
      );

      final versionData = versionV1.Version.fromBuffer(decrypted);
      if (versionData.version != 1) {
        if (mounted) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(AppLocalizations.of(context).scanErrorUnknownDataFormat),
            ),
          );
        }
        return;
      }

      final data = datenV1.QRData.fromBuffer(decrypted);
      widget.pupilsDetailsBloc.addAll(
        data.pupilDetails
            .map(
              (final m) => PupilDetails(
                id: m.id,
                name: m.name,
                group: m.group,
                schoolYear: m.schoolYear,
              ),
            )
            .toList(),
      );

      if (mounted) {
        Navigator.of(context).pop();
      }
    } catch (e, s) {
      debugPrint(e.toString());
      debugPrint(s.toString());
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(AppLocalizations.of(context).scanErrorBadData),
          ),
        );
      }
    }
  }

  @override
  Widget build(final BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context).scanTitle),
        ),
        body: Stack(
          children: [
            MobileScanner(
              controller: controller,
              onDetect: (final barcode, final arguments) {},
            ),
            Container(
              alignment: Alignment.bottomCenter,
              padding: const EdgeInsets.all(10),
              child: ElevatedButton(
                onPressed: () async {
                  final file = await ImagePicker().pickImage(
                    source: ImageSource.gallery,
                  );
                  if (file == null) {
                    return;
                  }
                  final success = await controller.analyzeImage(file.path);
                  if (!success && mounted) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(AppLocalizations.of(context).scanErrorNoQRCodeFound),
                      ),
                    );
                  }
                },
                child: Text(AppLocalizations.of(context).scanPickImage),
              ),
            ),
          ],
        ),
      );
}
