part of '../daten_app.dart';

class HomePage extends StatefulWidget {
  HomePage({
    required this.storage,
    required this.session,
    super.key,
  }) : assert(session.isAuthenticated, 'Session needs to be authenticated');

  final FlutterSecureStorage storage;
  final Session session;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final categoriesBloc = CategoriesBloc(widget.session);
  late final pupilsBloc = PupilsBloc(widget.session);
  late final pupilsDetailsBloc = PupilsDetailsBloc(widget.storage);

  @override
  void initState() {
    super.initState();

    categoriesBloc.categories.listen((final _) {/* To make the categories load */});

    pupilsBloc.errors.listen((final error) {
      ErrorWidget.showSnackbar(context, error);
    });
  }

  @override
  Widget build(final BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context).title),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            await Navigator.of(context).push(
              MaterialPageRoute(
                builder: (final _) => ScanPage(
                  pupilsDetailsBloc: pupilsDetailsBloc,
                ),
              ),
            );
          },
          child: const Icon(Icons.qr_code_scanner),
        ),
        body: StreamBuilder<List<PupilDetails>>(
          stream: pupilsDetailsBloc.pupilsDetails,
          builder: (final context, final pupilsDetailsSnapshot) => !pupilsDetailsSnapshot.hasData
              ? Container()
              : pupilsDetailsSnapshot.data!.isEmpty
                  ? Container(
                      padding: const EdgeInsets.all(30),
                      child: Center(
                        child: Text(
                          AppLocalizations.of(context).homeNoEntriesScanQRCodeToAddSome,
                          style: Theme.of(context).textTheme.headline6,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  : StreamBuilder<List<Pupil>>(
                      stream: pupilsBloc.pupils,
                      builder: (final context, final pupilsSnapshot) => SnapshotBuilder<List<Pupil>>(
                        snapshot: pupilsSnapshot,
                        onRetry: pupilsBloc.refresh,
                        builder: (final context, final pupils) => ListView(
                          padding: const EdgeInsets.all(10),
                          children: [
                            for (final pupilDetails in pupilsDetailsSnapshot.data!) ...[
                              if (pupils.where((final pupil) => pupil.id == pupilDetails.id).isNotEmpty) ...[
                                ListTile(
                                  title: Text(
                                    pupilDetails.name,
                                  ),
                                  onTap: () async {
                                    await Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (final context) => PupilOverviewPage(
                                          categories: categoriesBloc.categories.value,
                                          pupilDetails: pupilDetails,
                                          pupilsBloc: pupilsBloc,
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ],
                            ],
                          ],
                        ),
                      ),
                    ),
        ),
      );
}
