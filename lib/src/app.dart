part of 'daten_app.dart';

const _primaryColor = Color(0xFFFF7000);

class DatenApp extends StatefulWidget {
  const DatenApp({
    required this.storage,
    super.key,
  });

  final FlutterSecureStorage storage;

  @override
  State<DatenApp> createState() => _DatenAppState();
}

// ignore: prefer_mixin
class _DatenAppState extends State<DatenApp> with WidgetsBindingObserver {
  final _navigatorKey = GlobalKey<NavigatorState>();

  var _platformBrightness = WidgetsBinding.instance.window.platformBrightness;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);

    final sessionBloc = Provider.of<SessionBloc>(context, listen: false);
    sessionBloc.session.listen((final session) async {
      await _navigatorKey.currentState!.pushAndRemoveUntil(
        MaterialPageRoute(
          builder: (final context) => session.isAuthenticated
              ? HomePage(
                  storage: widget.storage,
                  session: session,
                )
              : const LoginPage(),
        ),
        (final route) => false,
      );
    });

    sessionBloc.errors.listen((final error) {
      ErrorWidget.showSnackbar(context, error);
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);

    super.dispose();
  }

  @override
  void didChangePlatformBrightness() {
    setState(() {
      _platformBrightness = WidgetsBinding.instance.window.platformBrightness;
    });

    super.didChangePlatformBrightness();
  }

  @override
  Widget build(final BuildContext context) => MaterialApp(
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        navigatorKey: _navigatorKey,
        theme: ThemeData(
          useMaterial3: true,
          brightness: _platformBrightness,
          colorScheme: _platformBrightness == Brightness.light
              ? const ColorScheme.light(
                  primary: _primaryColor,
                )
              : const ColorScheme.dark(
                  primary: _primaryColor,
                ),
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
              foregroundColor: Colors.white,
              backgroundColor: _primaryColor,
            ).copyWith(
              elevation: ButtonStyleButton.allOrNull(0),
            ),
          ),
          appBarTheme: const AppBarTheme(
            backgroundColor: _primaryColor,
          ),
          snackBarTheme: const SnackBarThemeData(
            behavior: SnackBarBehavior.floating,
          ),
          checkboxTheme: CheckboxThemeData(
            fillColor: MaterialStateProperty.resolveWith((final states) {
              if (states.contains(MaterialState.disabled)) {
                return Colors.black38;
              }

              return _primaryColor;
            }),
          ),
        ),
        home: Container(),
      );
}
