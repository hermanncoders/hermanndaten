// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Generator: RxBlocGeneratorForAnnotation
// **************************************************************************

part of 'session.dart';

/// Used as a contractor for the bloc, events and states classes
/// {@nodoc}
abstract class SessionBlocType extends RxBlocTypeBase {
  SessionBlocEvents get events;
  SessionBlocStates get states;
}

/// [$SessionBloc] extended by the [SessionBloc]
/// {@nodoc}
abstract class $SessionBloc extends RxBlocBase implements SessionBlocEvents, SessionBlocStates, SessionBlocType {
  final _compositeSubscription = CompositeSubscription();

  /// Тhe [Subject] where events sink to by calling [set]
  final _$setEvent = PublishSubject<Session>();

  /// The state of [session] implemented in [_mapToSessionState]
  late final BehaviorSubject<Session> _sessionState = _mapToSessionState();

  /// The state of [errors] implemented in [_mapToErrorsState]
  late final Stream<Exception> _errorsState = _mapToErrorsState();

  @override
  void set(Session session) => _$setEvent.add(session);

  @override
  BehaviorSubject<Session> get session => _sessionState;

  @override
  Stream<Exception> get errors => _errorsState;

  BehaviorSubject<Session> _mapToSessionState();

  Stream<Exception> _mapToErrorsState();

  @override
  SessionBlocEvents get events => this;

  @override
  SessionBlocStates get states => this;

  @override
  void dispose() {
    _$setEvent.close();
    _compositeSubscription.dispose();
    super.dispose();
  }
}
