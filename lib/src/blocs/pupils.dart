import 'dart:async';

import 'package:daten_app/src/models/development_goal.dart';
import 'package:daten_app/src/models/development_goal_category.dart';
import 'package:daten_app/src/models/development_goal_check.dart';
import 'package:daten_app/src/models/pupil.dart';
import 'package:daten_app/src/models/pupil_details.dart';
import 'package:daten_app/src/models/session.dart';
import 'package:daten_app/src/service.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';

part 'pupils.rxb.g.dart';

abstract class PupilsBlocEvents {
  void refresh();

  void addDevelopmentGoal({
    required final PupilDetails pupilDetails,
    required final DevelopmentGoalCategory category,
    required final String description,
    required final List<String> strategies,
  });

  void updateDevelopmentGoal({
    required final PupilDetails pupilDetails,
    required final DevelopmentGoal goal,
    required final bool achieved,
    required final String description,
    required final List<String> strategies,
    required final List<DevelopmentGoalCheck> checks,
  });
}

abstract class PupilsBlocStates {
  BehaviorSubject<List<Pupil>> get pupils;

  Stream<Exception> get errors;
}

@RxBloc()
class PupilsBloc extends $PupilsBloc {
  PupilsBloc(this._session) {
    _$refreshEvent.listen((final _) async => _loadPupils());

    _$addDevelopmentGoalEvent.listen(
      (final event) async => _wrapAction(
        ApiService.addDevelopmentGoal(
          session: _session,
          pupilId: event.pupilDetails.id,
          categoryId: event.category.id,
          description: event.description,
          strategies: event.strategies,
        ),
      ),
    );

    _$updateDevelopmentGoalEvent.listen(
      (final event) async => _wrapAction(
        ApiService.updateDevelopmentGoal(
          session: _session,
          pupilId: event.pupilDetails.id,
          goalId: event.goal.id!,
          achieved: event.achieved,
          description: event.description,
          strategies: event.strategies,
          checks: event.checks,
        ),
      ),
    );

    unawaited(_loadPupils());
  }

  Future _loadPupils() async {
    await _wrapAction(ApiService.getPupils(_session));
  }

  Future _wrapAction(final Future<List<Pupil>> future) async {
    await future.then(_pupilsSubject.add).onError((final error, final stackTrace) => _pupilsSubject.addError(error!));
  }

  final Session _session;

  final _pupilsSubject = BehaviorSubject<List<Pupil>>();
  final _errorsController = StreamController<Exception>();

  @override
  void dispose() {
    unawaited(_pupilsSubject.close());
    unawaited(_errorsController.close());
    super.dispose();
  }

  @override
  BehaviorSubject<List<Pupil>> _mapToPupilsState() => _pupilsSubject;

  @override
  Stream<Exception> _mapToErrorsState() => _errorsController.stream;
}
