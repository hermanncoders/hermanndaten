// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Generator: RxBlocGeneratorForAnnotation
// **************************************************************************

part of 'pupils_details.dart';

/// Used as a contractor for the bloc, events and states classes
/// {@nodoc}
abstract class PupilsDetailsBlocType extends RxBlocTypeBase {
  PupilsDetailsBlocEvents get events;
  PupilsDetailsBlocStates get states;
}

/// [$PupilsDetailsBloc] extended by the [PupilsDetailsBloc]
/// {@nodoc}
abstract class $PupilsDetailsBloc extends RxBlocBase
    implements PupilsDetailsBlocEvents, PupilsDetailsBlocStates, PupilsDetailsBlocType {
  final _compositeSubscription = CompositeSubscription();

  /// Тhe [Subject] where events sink to by calling [addAll]
  final _$addAllEvent = PublishSubject<List<PupilDetails>>();

  /// The state of [pupilsDetails] implemented in [_mapToPupilsDetailsState]
  late final BehaviorSubject<List<PupilDetails>> _pupilsDetailsState = _mapToPupilsDetailsState();

  @override
  void addAll(List<PupilDetails> pupilDetails) => _$addAllEvent.add(pupilDetails);

  @override
  BehaviorSubject<List<PupilDetails>> get pupilsDetails => _pupilsDetailsState;

  BehaviorSubject<List<PupilDetails>> _mapToPupilsDetailsState();

  @override
  PupilsDetailsBlocEvents get events => this;

  @override
  PupilsDetailsBlocStates get states => this;

  @override
  void dispose() {
    _$addAllEvent.close();
    _compositeSubscription.dispose();
    super.dispose();
  }
}
