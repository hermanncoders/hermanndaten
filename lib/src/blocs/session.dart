import 'dart:async';
import 'dart:convert';

import 'package:daten_app/src/models/session.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';

part 'session.rxb.g.dart';

abstract class SessionBlocEvents {
  void set(final Session session);
}

abstract class SessionBlocStates {
  BehaviorSubject<Session> get session;

  Stream<Exception> get errors;
}

@RxBloc()
class SessionBloc extends $SessionBloc {
  SessionBloc(this._storage) {
    _$setEvent.listen((final session) async {
      _sessionSubject.add(session);
      await _saveSession(session);
    });

    _loadSession();
  }

  void _loadSession() {
    unawaited(
      _storage.containsKey(key: _keySession).then((final exists) async {
        if (exists) {
          try {
            final session = Session.fromJson(
              json.decode((await _storage.read(key: _keySession))!) as Map<String, dynamic>,
            );

            // TODO: Enable validation
            /*
          if (JwtDecoder.isExpired(session.jwt!)) {
            _sessionSubject.add(Session());
            await _storage.delete(key: _keySession);

            return;
          }
          */

            _sessionSubject.add(session);
          } catch (e) {
            _errorsController.add(e as Exception);
            await _storage.delete(key: _keySession);
            _sessionSubject.add(Session());
          }
        } else {
          _sessionSubject.add(Session());
        }
      }),
    );
  }

  Future _saveSession(final Session session) async {
    await _storage.write(
      key: _keySession,
      value: json.encode(session.toJson()),
    );
  }

  final FlutterSecureStorage _storage;

  final _keySession = 'session';

  final _sessionSubject = BehaviorSubject<Session>();
  final _errorsController = StreamController<Exception>();

  @override
  void dispose() {
    unawaited(_sessionSubject.close());
    unawaited(_errorsController.close());
    super.dispose();
  }

  @override
  BehaviorSubject<Session> _mapToSessionState() => _sessionSubject;

  @override
  Stream<Exception> _mapToErrorsState() => _errorsController.stream;
}
