import 'dart:async';

import 'package:daten_app/src/models/development_goal_category.dart';
import 'package:daten_app/src/models/session.dart';
import 'package:daten_app/src/service.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';

part 'categories.rxb.g.dart';

abstract class CategoriesBlocEvents {
  void refresh();
}

abstract class CategoriesBlocStates {
  BehaviorSubject<List<DevelopmentGoalCategory>> get categories;
}

@RxBloc()
class CategoriesBloc extends $CategoriesBloc {
  CategoriesBloc(this._session) {
    _$refreshEvent.listen((final _) => _loadCategories());

    _loadCategories();
  }

  void _loadCategories() {
    unawaited(
      ApiService.getCategories(_session).then(_categoriesSubject.add).onError(
            (final error, final stackTrace) => _categoriesSubject.addError(error!),
          ),
    );
  }

  final Session _session;

  final _categoriesSubject = BehaviorSubject<List<DevelopmentGoalCategory>>();

  @override
  void dispose() {
    unawaited(_categoriesSubject.close());
    super.dispose();
  }

  @override
  BehaviorSubject<List<DevelopmentGoalCategory>> _mapToCategoriesState() => _categoriesSubject;
}
