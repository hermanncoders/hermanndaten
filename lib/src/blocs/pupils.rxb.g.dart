// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Generator: RxBlocGeneratorForAnnotation
// **************************************************************************

part of 'pupils.dart';

/// Used as a contractor for the bloc, events and states classes
/// {@nodoc}
abstract class PupilsBlocType extends RxBlocTypeBase {
  PupilsBlocEvents get events;
  PupilsBlocStates get states;
}

/// [$PupilsBloc] extended by the [PupilsBloc]
/// {@nodoc}
abstract class $PupilsBloc extends RxBlocBase implements PupilsBlocEvents, PupilsBlocStates, PupilsBlocType {
  final _compositeSubscription = CompositeSubscription();

  /// Тhe [Subject] where events sink to by calling [refresh]
  final _$refreshEvent = PublishSubject<void>();

  /// Тhe [Subject] where events sink to by calling [addDevelopmentGoal]
  final _$addDevelopmentGoalEvent = PublishSubject<_AddDevelopmentGoalEventArgs>();

  /// Тhe [Subject] where events sink to by calling [updateDevelopmentGoal]
  final _$updateDevelopmentGoalEvent = PublishSubject<_UpdateDevelopmentGoalEventArgs>();

  /// The state of [pupils] implemented in [_mapToPupilsState]
  late final BehaviorSubject<List<Pupil>> _pupilsState = _mapToPupilsState();

  /// The state of [errors] implemented in [_mapToErrorsState]
  late final Stream<Exception> _errorsState = _mapToErrorsState();

  @override
  void refresh() => _$refreshEvent.add(null);

  @override
  void addDevelopmentGoal({
    required PupilDetails pupilDetails,
    required DevelopmentGoalCategory category,
    required String description,
    required List<String> strategies,
  }) =>
      _$addDevelopmentGoalEvent.add(_AddDevelopmentGoalEventArgs(
        pupilDetails: pupilDetails,
        category: category,
        description: description,
        strategies: strategies,
      ));

  @override
  void updateDevelopmentGoal({
    required PupilDetails pupilDetails,
    required DevelopmentGoal goal,
    required bool achieved,
    required String description,
    required List<String> strategies,
    required List<DevelopmentGoalCheck> checks,
  }) =>
      _$updateDevelopmentGoalEvent.add(_UpdateDevelopmentGoalEventArgs(
        pupilDetails: pupilDetails,
        goal: goal,
        achieved: achieved,
        description: description,
        strategies: strategies,
        checks: checks,
      ));

  @override
  BehaviorSubject<List<Pupil>> get pupils => _pupilsState;

  @override
  Stream<Exception> get errors => _errorsState;

  BehaviorSubject<List<Pupil>> _mapToPupilsState();

  Stream<Exception> _mapToErrorsState();

  @override
  PupilsBlocEvents get events => this;

  @override
  PupilsBlocStates get states => this;

  @override
  void dispose() {
    _$refreshEvent.close();
    _$addDevelopmentGoalEvent.close();
    _$updateDevelopmentGoalEvent.close();
    _compositeSubscription.dispose();
    super.dispose();
  }
}

/// Helps providing the arguments in the [Subject.add] for
/// [PupilsBlocEvents.addDevelopmentGoal] event
class _AddDevelopmentGoalEventArgs {
  const _AddDevelopmentGoalEventArgs({
    required this.pupilDetails,
    required this.category,
    required this.description,
    required this.strategies,
  });

  final PupilDetails pupilDetails;

  final DevelopmentGoalCategory category;

  final String description;

  final List<String> strategies;
}

/// Helps providing the arguments in the [Subject.add] for
/// [PupilsBlocEvents.updateDevelopmentGoal] event
class _UpdateDevelopmentGoalEventArgs {
  const _UpdateDevelopmentGoalEventArgs({
    required this.pupilDetails,
    required this.goal,
    required this.achieved,
    required this.description,
    required this.strategies,
    required this.checks,
  });

  final PupilDetails pupilDetails;

  final DevelopmentGoal goal;

  final bool achieved;

  final String description;

  final List<String> strategies;

  final List<DevelopmentGoalCheck> checks;
}
