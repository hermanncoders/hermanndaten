import 'dart:async';

import 'package:daten_app/src/models/session.dart';
import 'package:daten_app/src/service.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';

part 'login.rxb.g.dart';

abstract class LoginBlocEvents {
  void login(final String username, final String password);
}

abstract class LoginBlocStates {
  BehaviorSubject<Session> get session;

  Stream<Exception> get errors;
}

@RxBloc()
class LoginBloc extends $LoginBloc {
  LoginBloc() {
    _$loginEvent.listen((final event) => _login(event.username, event.password));
  }

  void _login(final String username, final String password) {
    unawaited(
      ApiService.login(username, password).then(_sessionSubject.add).onError(
            (final error, final stackTrace) => _errorsController.addError(error!),
          ),
    );
  }

  final _sessionSubject = BehaviorSubject<Session>();
  final _errorsController = StreamController<Exception>();

  @override
  void dispose() {
    unawaited(_sessionSubject.close());
    unawaited(_errorsController.close());
    super.dispose();
  }

  @override
  BehaviorSubject<Session> _mapToSessionState() => _sessionSubject;

  @override
  Stream<Exception> _mapToErrorsState() => _errorsController.stream;
}
