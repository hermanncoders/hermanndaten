// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Generator: RxBlocGeneratorForAnnotation
// **************************************************************************

part of 'login.dart';

/// Used as a contractor for the bloc, events and states classes
/// {@nodoc}
abstract class LoginBlocType extends RxBlocTypeBase {
  LoginBlocEvents get events;
  LoginBlocStates get states;
}

/// [$LoginBloc] extended by the [LoginBloc]
/// {@nodoc}
abstract class $LoginBloc extends RxBlocBase implements LoginBlocEvents, LoginBlocStates, LoginBlocType {
  final _compositeSubscription = CompositeSubscription();

  /// Тhe [Subject] where events sink to by calling [login]
  final _$loginEvent = PublishSubject<_LoginEventArgs>();

  /// The state of [session] implemented in [_mapToSessionState]
  late final BehaviorSubject<Session> _sessionState = _mapToSessionState();

  /// The state of [errors] implemented in [_mapToErrorsState]
  late final Stream<Exception> _errorsState = _mapToErrorsState();

  @override
  void login(
    String username,
    String password,
  ) =>
      _$loginEvent.add(_LoginEventArgs(
        username,
        password,
      ));

  @override
  BehaviorSubject<Session> get session => _sessionState;

  @override
  Stream<Exception> get errors => _errorsState;

  BehaviorSubject<Session> _mapToSessionState();

  Stream<Exception> _mapToErrorsState();

  @override
  LoginBlocEvents get events => this;

  @override
  LoginBlocStates get states => this;

  @override
  void dispose() {
    _$loginEvent.close();
    _compositeSubscription.dispose();
    super.dispose();
  }
}

/// Helps providing the arguments in the [Subject.add] for
/// [LoginBlocEvents.login] event
class _LoginEventArgs {
  const _LoginEventArgs(
    this.username,
    this.password,
  );

  final String username;

  final String password;
}
