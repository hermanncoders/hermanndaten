import 'dart:async';
import 'dart:convert';

import 'package:daten_app/src/models/pupil_details.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';

part 'pupils_details.rxb.g.dart';

abstract class PupilsDetailsBlocEvents {
  void addAll(final List<PupilDetails> pupilDetails);
}

abstract class PupilsDetailsBlocStates {
  BehaviorSubject<List<PupilDetails>> get pupilsDetails;
}

@RxBloc()
class PupilsDetailsBloc extends $PupilsDetailsBloc {
  PupilsDetailsBloc(this._storage) {
    _$addAllEvent.listen((final pupilsDetails) async {
      // This way it's possible to overwrite details by scanning a new QR code
      final newPupilDetails = <String, PupilDetails>{};
      for (final pupilDetails in [..._pupilsDetailsSubject.value, ...pupilsDetails]) {
        newPupilDetails[pupilDetails.id] = pupilDetails;
      }

      _pupilsDetailsSubject.add(newPupilDetails.values.toList());
      await _savePupilsDetails(newPupilDetails.values.toList());
    });

    _loadPupilsDetails();
  }

  void _loadPupilsDetails() {
    unawaited(
      _storage.containsKey(key: _keyPupilsDetails).then((final exists) async {
        if (exists) {
          _pupilsDetailsSubject.add(
            (json.decode((await _storage.read(key: _keyPupilsDetails))!) as List)
                .cast<Map<String, dynamic>>()
                .map(PupilDetails.fromJson)
                .toList(),
          );
        } else {
          _pupilsDetailsSubject.add([]);
        }
      }),
    );
  }

  Future _savePupilsDetails(final List<PupilDetails> pupilsDetails) async {
    await _storage.write(
      key: _keyPupilsDetails,
      value: json.encode(pupilsDetails.map((final m) => m.toJson()).toList()),
    );
  }

  final FlutterSecureStorage _storage;

  final _keyPupilsDetails = 'pupils-details';

  final _pupilsDetailsSubject = BehaviorSubject<List<PupilDetails>>();

  @override
  void dispose() {
    unawaited(_pupilsDetailsSubject.close());
    super.dispose();
  }

  @override
  BehaviorSubject<List<PupilDetails>> _mapToPupilsDetailsState() => _pupilsDetailsSubject;
}
