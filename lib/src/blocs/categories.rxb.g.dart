// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Generator: RxBlocGeneratorForAnnotation
// **************************************************************************

part of 'categories.dart';

/// Used as a contractor for the bloc, events and states classes
/// {@nodoc}
abstract class CategoriesBlocType extends RxBlocTypeBase {
  CategoriesBlocEvents get events;
  CategoriesBlocStates get states;
}

/// [$CategoriesBloc] extended by the [CategoriesBloc]
/// {@nodoc}
abstract class $CategoriesBloc extends RxBlocBase
    implements CategoriesBlocEvents, CategoriesBlocStates, CategoriesBlocType {
  final _compositeSubscription = CompositeSubscription();

  /// Тhe [Subject] where events sink to by calling [refresh]
  final _$refreshEvent = PublishSubject<void>();

  /// The state of [categories] implemented in [_mapToCategoriesState]
  late final BehaviorSubject<List<DevelopmentGoalCategory>> _categoriesState = _mapToCategoriesState();

  @override
  void refresh() => _$refreshEvent.add(null);

  @override
  BehaviorSubject<List<DevelopmentGoalCategory>> get categories => _categoriesState;

  BehaviorSubject<List<DevelopmentGoalCategory>> _mapToCategoriesState();

  @override
  CategoriesBlocEvents get events => this;

  @override
  CategoriesBlocStates get states => this;

  @override
  void dispose() {
    _$refreshEvent.close();
    _compositeSubscription.dispose();
    super.dispose();
  }
}
