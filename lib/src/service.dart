import 'dart:convert';
import 'dart:math';

import 'package:daten_app/src/models/development_goal.dart';
import 'package:daten_app/src/models/development_goal_category.dart';
import 'package:daten_app/src/models/development_goal_check.dart';
import 'package:daten_app/src/models/pupil.dart';
import 'package:daten_app/src/models/session.dart';
import 'package:flutter/services.dart';

class ApiService {
  static List<Pupil>? _statePupils;

  static Future<Session> login(final String username, final String password) async {
    await Future.delayed(const Duration(milliseconds: 100));
    return Session(
      username: username,
      jwt: '',
    );
  }

  static Future<List<DevelopmentGoalCategory>> getCategories(final Session session) async {
    await Future.delayed(const Duration(milliseconds: 100));

    return (json.decode(await rootBundle.loadString('assets/categories.json')) as List)
        .map((final c) => DevelopmentGoalCategory.fromJson(c as Map<String, dynamic>))
        .toList();
  }

  static Future<List<Pupil>> getPupils(final Session session) async {
    await Future.delayed(const Duration(milliseconds: 100));

    if (_statePupils != null) {
      return _statePupils!;
    }

    return _statePupils = [
      Pupil(
        id: '123',
        developmentGoals: [],
      ),
      Pupil(
        id: '456',
        developmentGoals: [],
      ),
      Pupil(
        id: '789',
        developmentGoals: [],
      ),
    ];
  }

  static Future<List<Pupil>> addDevelopmentGoal({
    required final Session session,
    required final String pupilId,
    required final int categoryId,
    required final String description,
    required final List<String> strategies,
  }) async {
    await Future.delayed(const Duration(milliseconds: 100));

    for (final pupil in _statePupils!) {
      if (pupil.id == pupilId) {
        pupil.developmentGoals.add(
          DevelopmentGoal(
            id: _randomID(),
            categoryID: categoryId,
            creator: session.username!,
            created: DateTime.now(),
            achieved: false,
            description: description,
            strategies: strategies,
            checks: [],
          ),
        );
      }
    }

    return getPupils(session);
  }

  static Future<List<Pupil>> updateDevelopmentGoal({
    required final Session session,
    required final String pupilId,
    required final int goalId,
    required final bool achieved,
    required final String description,
    required final List<String> strategies,
    required final List<DevelopmentGoalCheck> checks,
  }) async {
    await Future.delayed(const Duration(milliseconds: 100));

    for (final pupil in _statePupils!) {
      if (pupil.id == pupilId) {
        for (var i = 0; i < pupil.developmentGoals.length; i++) {
          final developmentGoal = pupil.developmentGoals[i];
          if (developmentGoal.id == goalId) {
            pupil.developmentGoals[i] = DevelopmentGoal(
              id: goalId,
              categoryID: developmentGoal.categoryID,
              creator: developmentGoal.creator,
              created: developmentGoal.created,
              achieved: achieved,
              description: description,
              strategies: strategies,
              checks: checks,
            );
          }
        }
      }
    }

    return getPupils(session);
  }

  static int _randomID() => Random().nextInt(pow(2, 32).toInt());
}
