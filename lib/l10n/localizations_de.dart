import 'localizations.dart';

/// The translations for German (`de`).
class AppLocalizationsDe extends AppLocalizations {
  AppLocalizationsDe([String locale = 'de']) : super(locale);

  @override
  String get title => 'Hermann-Daten';

  @override
  String get loginUsername => 'Name';

  @override
  String get loginPassword => 'Passwort';

  @override
  String get loginSubmit => 'Anmelden';

  @override
  String get loginAgain => 'Wieder anmelden';

  @override
  String get errorEmptyField => 'Dieses Feld darf nicht leer sein';

  @override
  String get errorCredentialsWrong => 'Anmeldedaten falsch';

  @override
  String get errorUnknown => 'Ein unbekannter Fehler ist aufgetreten.';

  @override
  String get retry => 'Erneut versuchen';

  @override
  String get scanTitle => 'QR-Code scannen';

  @override
  String get scanPickImage => 'QR-Code aus Fotos auswählen';

  @override
  String get scanErrorNoQRCodeFound => 'Es konnte kein QR-Code gefunden werden.';

  @override
  String get scanError => 'Fehler beim scannen';

  @override
  String get scanErrorBadData => 'Die im QR-Code enthaltenen Daten konnten nicht ausgewertet werden';

  @override
  String get scanErrorUnknownDataFormat => 'Die Version der Daten im QR-Code wird in dieser Version der App nicht verstanden';

  @override
  String get homeNoEntriesScanQRCodeToAddSome => 'Keine Einträge vorhanden. Scanne einen QR-Code um welche hinzu zu fügen.';

  @override
  String get upsertDevelopmentGoalNewTitle => 'Neues Förderziel';

  @override
  String get upsertDevelopmentGoalEditTitle => 'Förderziel bearbeiten';

  @override
  String get upsertDevelopmentGoalCategory => 'Kategorie';

  @override
  String get upsertDevelopmentGoalDescription => 'Beschreibung';

  @override
  String get upsertDevelopmentGoalStrategies => 'Förderstrategien';

  @override
  String get upsertDevelopmentGoalStrategiesAdd => 'Weitere Strategie hinzufügen';

  @override
  String get upsertDevelopmentGoalAchieved => 'Förderziel erreicht';

  @override
  String get upsertDevelopmentGoalSubmit => 'Speichern';

  @override
  String get developmentGoalCategorySelectNoCategory => 'Keine Kategorie';
}
