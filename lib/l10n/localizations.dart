import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'localizations_de.dart';

/// Callers can lookup localized strings with an instance of AppLocalizations
/// returned by `AppLocalizations.of(context)`.
///
/// Applications need to include `AppLocalizations.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'l10n/localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: AppLocalizations.localizationsDelegates,
///   supportedLocales: AppLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the AppLocalizations.supportedLocales
/// property.
abstract class AppLocalizations {
  AppLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations)!;
  }

  static const LocalizationsDelegate<AppLocalizations> delegate = _AppLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('de')
  ];

  /// No description provided for @title.
  ///
  /// In de, this message translates to:
  /// **'Hermann-Daten'**
  String get title;

  /// No description provided for @loginUsername.
  ///
  /// In de, this message translates to:
  /// **'Name'**
  String get loginUsername;

  /// No description provided for @loginPassword.
  ///
  /// In de, this message translates to:
  /// **'Passwort'**
  String get loginPassword;

  /// No description provided for @loginSubmit.
  ///
  /// In de, this message translates to:
  /// **'Anmelden'**
  String get loginSubmit;

  /// No description provided for @loginAgain.
  ///
  /// In de, this message translates to:
  /// **'Wieder anmelden'**
  String get loginAgain;

  /// No description provided for @errorEmptyField.
  ///
  /// In de, this message translates to:
  /// **'Dieses Feld darf nicht leer sein'**
  String get errorEmptyField;

  /// No description provided for @errorCredentialsWrong.
  ///
  /// In de, this message translates to:
  /// **'Anmeldedaten falsch'**
  String get errorCredentialsWrong;

  /// No description provided for @errorUnknown.
  ///
  /// In de, this message translates to:
  /// **'Ein unbekannter Fehler ist aufgetreten.'**
  String get errorUnknown;

  /// No description provided for @retry.
  ///
  /// In de, this message translates to:
  /// **'Erneut versuchen'**
  String get retry;

  /// No description provided for @scanTitle.
  ///
  /// In de, this message translates to:
  /// **'QR-Code scannen'**
  String get scanTitle;

  /// No description provided for @scanPickImage.
  ///
  /// In de, this message translates to:
  /// **'QR-Code aus Fotos auswählen'**
  String get scanPickImage;

  /// No description provided for @scanErrorNoQRCodeFound.
  ///
  /// In de, this message translates to:
  /// **'Es konnte kein QR-Code gefunden werden.'**
  String get scanErrorNoQRCodeFound;

  /// No description provided for @scanError.
  ///
  /// In de, this message translates to:
  /// **'Fehler beim scannen'**
  String get scanError;

  /// No description provided for @scanErrorBadData.
  ///
  /// In de, this message translates to:
  /// **'Die im QR-Code enthaltenen Daten konnten nicht ausgewertet werden'**
  String get scanErrorBadData;

  /// No description provided for @scanErrorUnknownDataFormat.
  ///
  /// In de, this message translates to:
  /// **'Die Version der Daten im QR-Code wird in dieser Version der App nicht verstanden'**
  String get scanErrorUnknownDataFormat;

  /// No description provided for @homeNoEntriesScanQRCodeToAddSome.
  ///
  /// In de, this message translates to:
  /// **'Keine Einträge vorhanden. Scanne einen QR-Code um welche hinzu zu fügen.'**
  String get homeNoEntriesScanQRCodeToAddSome;

  /// No description provided for @upsertDevelopmentGoalNewTitle.
  ///
  /// In de, this message translates to:
  /// **'Neues Förderziel'**
  String get upsertDevelopmentGoalNewTitle;

  /// No description provided for @upsertDevelopmentGoalEditTitle.
  ///
  /// In de, this message translates to:
  /// **'Förderziel bearbeiten'**
  String get upsertDevelopmentGoalEditTitle;

  /// No description provided for @upsertDevelopmentGoalCategory.
  ///
  /// In de, this message translates to:
  /// **'Kategorie'**
  String get upsertDevelopmentGoalCategory;

  /// No description provided for @upsertDevelopmentGoalDescription.
  ///
  /// In de, this message translates to:
  /// **'Beschreibung'**
  String get upsertDevelopmentGoalDescription;

  /// No description provided for @upsertDevelopmentGoalStrategies.
  ///
  /// In de, this message translates to:
  /// **'Förderstrategien'**
  String get upsertDevelopmentGoalStrategies;

  /// No description provided for @upsertDevelopmentGoalStrategiesAdd.
  ///
  /// In de, this message translates to:
  /// **'Weitere Strategie hinzufügen'**
  String get upsertDevelopmentGoalStrategiesAdd;

  /// No description provided for @upsertDevelopmentGoalAchieved.
  ///
  /// In de, this message translates to:
  /// **'Förderziel erreicht'**
  String get upsertDevelopmentGoalAchieved;

  /// No description provided for @upsertDevelopmentGoalSubmit.
  ///
  /// In de, this message translates to:
  /// **'Speichern'**
  String get upsertDevelopmentGoalSubmit;

  /// No description provided for @developmentGoalCategorySelectNoCategory.
  ///
  /// In de, this message translates to:
  /// **'Keine Kategorie'**
  String get developmentGoalCategorySelectNoCategory;
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(lookupAppLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['de'].contains(locale.languageCode);

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}

AppLocalizations lookupAppLocalizations(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'de': return AppLocalizationsDe();
  }

  throw FlutterError(
    'AppLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
