///
//  Generated code. Do not modify.
//  source: hermann_daten/v1/hermann_daten.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class QRData extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'QRData', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'hermann_daten.v1'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'version', $pb.PbFieldType.OU3)
    ..pc<PupilDetails>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pupilDetails', $pb.PbFieldType.PM, subBuilder: PupilDetails.create)
    ..hasRequiredFields = false
  ;

  QRData._() : super();
  factory QRData({
    $core.int? version,
    $core.Iterable<PupilDetails>? pupilDetails,
  }) {
    final _result = create();
    if (version != null) {
      _result.version = version;
    }
    if (pupilDetails != null) {
      _result.pupilDetails.addAll(pupilDetails);
    }
    return _result;
  }
  factory QRData.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QRData.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QRData clone() => QRData()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QRData copyWith(void Function(QRData) updates) => super.copyWith((message) => updates(message as QRData)) as QRData; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static QRData create() => QRData._();
  QRData createEmptyInstance() => create();
  static $pb.PbList<QRData> createRepeated() => $pb.PbList<QRData>();
  @$core.pragma('dart2js:noInline')
  static QRData getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QRData>(create);
  static QRData? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get version => $_getIZ(0);
  @$pb.TagNumber(1)
  set version($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasVersion() => $_has(0);
  @$pb.TagNumber(1)
  void clearVersion() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<PupilDetails> get pupilDetails => $_getList(1);
}

class PupilDetails extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PupilDetails', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'hermann_daten.v1'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'group')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'schoolYear')
    ..hasRequiredFields = false
  ;

  PupilDetails._() : super();
  factory PupilDetails({
    $core.String? id,
    $core.String? name,
    $core.String? group,
    $core.String? schoolYear,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (name != null) {
      _result.name = name;
    }
    if (group != null) {
      _result.group = group;
    }
    if (schoolYear != null) {
      _result.schoolYear = schoolYear;
    }
    return _result;
  }
  factory PupilDetails.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PupilDetails.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PupilDetails clone() => PupilDetails()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PupilDetails copyWith(void Function(PupilDetails) updates) => super.copyWith((message) => updates(message as PupilDetails)) as PupilDetails; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PupilDetails create() => PupilDetails._();
  PupilDetails createEmptyInstance() => create();
  static $pb.PbList<PupilDetails> createRepeated() => $pb.PbList<PupilDetails>();
  @$core.pragma('dart2js:noInline')
  static PupilDetails getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PupilDetails>(create);
  static PupilDetails? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get group => $_getSZ(2);
  @$pb.TagNumber(3)
  set group($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasGroup() => $_has(2);
  @$pb.TagNumber(3)
  void clearGroup() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get schoolYear => $_getSZ(3);
  @$pb.TagNumber(4)
  set schoolYear($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasSchoolYear() => $_has(3);
  @$pb.TagNumber(4)
  void clearSchoolYear() => clearField(4);
}

