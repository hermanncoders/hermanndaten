///
//  Generated code. Do not modify.
//  source: hermann_daten/v1/hermann_daten.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use qRDataDescriptor instead')
const QRData$json = const {
  '1': 'QRData',
  '2': const [
    const {'1': 'version', '3': 1, '4': 1, '5': 13, '10': 'version'},
    const {'1': 'pupil_details', '3': 2, '4': 3, '5': 11, '6': '.hermann_daten.v1.PupilDetails', '10': 'pupilDetails'},
  ],
};

/// Descriptor for `QRData`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List qRDataDescriptor = $convert.base64Decode('CgZRUkRhdGESGAoHdmVyc2lvbhgBIAEoDVIHdmVyc2lvbhJDCg1wdXBpbF9kZXRhaWxzGAIgAygLMh4uaGVybWFubl9kYXRlbi52MS5QdXBpbERldGFpbHNSDHB1cGlsRGV0YWlscw==');
@$core.Deprecated('Use pupilDetailsDescriptor instead')
const PupilDetails$json = const {
  '1': 'PupilDetails',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'group', '3': 3, '4': 1, '5': 9, '10': 'group'},
    const {'1': 'school_year', '3': 4, '4': 1, '5': 9, '10': 'schoolYear'},
  ],
};

/// Descriptor for `PupilDetails`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pupilDetailsDescriptor = $convert.base64Decode('CgxQdXBpbERldGFpbHMSDgoCaWQYASABKAlSAmlkEhIKBG5hbWUYAiABKAlSBG5hbWUSFAoFZ3JvdXAYAyABKAlSBWdyb3VwEh8KC3NjaG9vbF95ZWFyGAQgASgJUgpzY2hvb2xZZWFy');
