import 'package:daten_app/src/blocs/session.dart';
import 'package:daten_app/src/daten_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();

  const storage = FlutterSecureStorage();

  final sessionBloc = SessionBloc(storage);
  runApp(
    Provider<SessionBloc>(
      create: (final _) => sessionBloc,
      child: const DatenApp(
        storage: storage,
      ),
    ),
  );
}
