#!/bin/bash
set -euxo pipefail

clang-format --verbose --Werror -i proto/hermann_daten/v1/hermann_daten.proto
clang-format --verbose --Werror -i proto/version/v1/version.proto

fvm dart format --fix --line-length 120 lib/src
