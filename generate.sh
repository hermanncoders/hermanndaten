#!/bin/bash
set -euxo pipefail
cd "$(dirname "$0")"

fvm flutter gen-l10n
fvm flutter pub run build_runner build --delete-conflicting-outputs

./format.sh

(cd proto && buf build && buf generate)

for file in testdata/*.csv; do
  fvm dart bin/encrypt.dart "$file" "$file.bin"
  qrencode -o "$file.png" -r "$file.bin"
done