# Proto

Hier befinden sich die Protobuf-Definitionen der QR-Codes.  
Das `version/v1` Protobuf wird benutzt um die Version der QR-Codes herauszufinden.
Dadurch lassen sich später weitere Versionen hinzufügen.
Das `hermann_daten/v1` Protobuf ist die aktuelle Version der Definition.